In this exercise, we will create a new branch, make some commits on it and then merge it with main branch.

1/ To update local branch list, use 'git fetch'. Sometimes branch on remote won't be accessible in local repo because branch list was not updated yet.

2/ To create a new branch with current commit as its origin, use 'git checkout -b [name_of_branch]'. You can see what it does with 'git log'.

3/ To move between branches, use the same command without the flag 'git checkout [name_of_branch]'.

4/ Sometimes, unstagged changes will prevent you from moving to another branch. By using the stash, you can keep your work uncommitted.
The stash works similarly to a stack, to add your unstagged work, use 'git stash'. To retrieve it, use 'git stash pop' and 'git stash list'
will display a list of all stashed elements. I recommend to use it only for switching branches and have only a single level of stash by branch.

5/ Whenever a new branch is created, you need to use a flag for the first push. Git will tell you if you forget: 'git push --set-upstream [name_of_the_branch]'

6/ Create a new branch, make a commit modifying file.txt 'text' to 'TEXT'.
Move to branch main, make a commit modifying file.txt 'here' to 'there'.

7/ There is two ways to combine two branches and update them with codes on another branch:
'git merge [branch_name]' or 'git rebase [branch_name]

When you merge a branch, it will apply all commits from this branch to current branch.
When you rebase a branch, it will apply all commits from current branch to this branch.
In both cases, only current branch is modified.

It is recommended to merge on main branch and rebase on other branches. It will make commit history easier to read.

8/ Whenever you merge or rebase, if the same line was modified on both side compared to the common commit, a conflict happens.
Try to merge, master with the new branch you created.

Using the command 'git diff', we obtain this:

++<<<<<<< HEAD
+Text there.
++=======
+TEXT here.
++>>>>>>> other

To solve the conflict, remove all extra lines from git and change the line so that it contains what you want:

TEXT there.

Use 'git add [file]' to add all files where conflicts were solved.
To finish merging/rebasing current commit with conflict, use 'git merge --continue' or 'git rebase --continue' depending on if you were in the middle of
merging or rebasing. Once done, you can push it.
If something went wrong, it is possible to abort the whole operation with the flag 'git merge/rebase --abort'.

9/ To throw everything and get back to a commit, you can use 'git reset --hard [commit_id]'.
A commit id can be a tag for a commit such as 'master' or the hash that can be found using 'git log' on the left side.
You can also move between commits by specifying the id of the commit with 'git checkout [commit_id]'.
