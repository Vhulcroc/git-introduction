# Git introduction

## Description

This repository contains ressources to get basic understanding of git command line.

## Getting started

To retrieve all files, clone this project by running the following command:

```bash
git clone https://gitlab.com/Vhulcroc/git-introduction
```
Inside the root directory, each folder corresponds to an exercise. These folders contains a 'subject.txt' which describes commands to run and what to do. The script 'script.sh' should be run in order to initialize the local git folder for the exercise.

To restart an exercise, you may use the following script in root directory:

```bash
./clean.sh [path_to_exercise_folder]
```

This command removes all untracked files from current directory.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
