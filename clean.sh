#! /bin/sh

if [ $# -ne 1 ]; then
    echo "Usage: $0 [path_to_folder]"
    exit 1
fi

cd $1
rm -rf .git/
git clean -df
cd -
