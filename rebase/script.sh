#! /bin/sh

git init
cp ../ignore_list.txt .gitignore
echo "Text here." > file.txt
git add file.txt
git add .gitignore
git commit -m "This is the first commit"
git checkout -b alt_branch
echo "Hello" > hello.txt
git add hello.txt
git commit -m "hello.txt added"
git checkout master
echo "World" > world.txt
git add world.txt
git commit -m "world.txt added"
