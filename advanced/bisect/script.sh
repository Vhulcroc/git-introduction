#! /bin/sh

git init
cp ../ignore_list.txt .gitignore
echo "Text here." > file.txt
git add file.txt
git add .gitignore
git commit -m "This is the first commit"

for i in {0..20}; do
    echo "Text here." > file$i.txt
    git add file$i.txt
    git commit -m "file$i.txt was added"
done
